import java.io.Serializable;
import java.util.Date;
public class Contacto extends Persona implements Serializable
{
     String email;
     String telefono;
    
     public Contacto(String a, Date b){
            super(a,b);
        }
        
     public Contacto(String a, String c, String d){
            super(a);
            this.email = c;
            this.telefono = d;
        }   
        
     public Contacto(String a, Date b, String c, String d){
            super(a,b);
            this.email = c;
            this.telefono = d;
        }
        
     public String getEmail(){
           return this.email;
        }
     public void setEmail(String a){
            this.email = a;
        }
        
     public String getTelefono(){
           return this.telefono;
        }
        public void setTelefono(long a){
            this.telefono = a;
        }
        
        public String toString(){
           return nombre + " - "+fdn+" - "+email+" - "+telefono;
        }

        public boolean equals(Contacto obj){
            return  this.telefono == obj.telefono;
        }
}
