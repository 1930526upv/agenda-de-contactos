public class ContactoNode {
    Contacto data;
    ContactoNode next;

    public ContactoNode(Contacto da, ContactoNode n){
        data = da;
        next = n;
    }
    public ContactoNode(Contacto da){
        this(da, null);
    }
}
