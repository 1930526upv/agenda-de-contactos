public class ContactoSLL {
    ContactoNode head;
    ContactoNode tail;

    public ContactoSLL(){
        head = tail = null;
    }

    public boolean isEmpty(){
        return head == null;
    }

    public void addToHead(Contacto da){
        head = new ContactoNode(da, head);
        if(tail == null)
            tail = head;
    }

    public void addToTail(Contacto da){
        if( !isEmpty() ){
            tail.next = new ContactoNode(da);
            tail = tail.next;
        }
        else
            head = tail = new ContactoNode(da);
    }

    public Contacto deleteFromHead(){
        Contacto el = tail.data;
        if( head == tail )//Solo hay un nodo en la SLL
            head = tail = null;
        else//si hay mas de un nodo
            head = head.next;
        return el;
    }

    public Contacto deleteFromTail(){
        Contacto el = tail.data;
        if( head == tail )//Solo hay un nodo en la SLL
            head = tail = null;
        else{//si hay mas de un nodo
            ContactoNode tmp;
            for( tmp = head; tmp.next != tail; tmp = tmp.next );
            tail = tmp;
            tail.next = null;
        }
        return el;
    }

    public void printAll(){
        for( ContactoNode tmp = head; tmp != null; tmp = tmp.next )
            System.out.print( tmp.data + "   " );
    }

    public boolean isInList( Contacto el ){
        ContactoNode tmp;
        for(tmp = head; tmp != null && tmp.data != el; tmp = tmp.next );
        return tmp != null;
    }

    public void delete(Contacto el){//Find first node "el" and delete it
        if( !isEmpty() )
            if( head == tail && head.data == el )
                head = tail = null;
            else if( el == head.data )//Si hay mas de un nodo en SLL
                head = head.next;//Se borro el nodo
        else{
            ContactoNode pred, tmp;//Si el elemento no esta en el nodo HEAD
            for(
                pred = head, tmp = head.next;
                tmp != null && tmp.data != el;
                pred = pred.next, tmp = tmp.next
            );
            if( tmp != null ){//el elemento se encontro
                pred.next = tmp.next;
                if( tmp == tail )//si el elemento se hallo en tail
                    tail = pred;
            }
        }
    }
}
