import java.text.SimpleDateFormat;
import java.util.*;

public class Agenda {
    public static void main(String[] args) {
        //Crear Borrar Actualizar Buscar
        Scanner sc = new Scanner(System.in);
        ContactoSLL agenda = new ContactoSLL();
        Contacto contacto;
        String nombre, email, telefono;
        Date  fdn;
        int action = 0;
        while(true){
            System.out.println("AGENDA\n");
            System.out.println("Que desea realizar?");
            System.out.println("1) Crear\n2)Borrar\n3)Actualizar\n4)Buscar\n5)Salir");
            System.out.println("Ingrese el numero>>>>");
            try{
                action = sc.nextInt();
            }
            catch(Exception e){
                System.out.println("Ingrese un numero correcto porfavor");
            }
            switch(action){
                case 1://Crear
                    String tmp;
                    System.out.println("Ingrese el nombre del contacto");
                    try{
                        tmp = sc.nextLine();
                    }catch(Exception e){System.out.println("Error");}
                    nombre = tmp;
                    tmp = null;
                    System.out.println("Ingrese la fecha de nacimiento del contacto");
                    System.out.println("En formato: dd/MM/yyyy");
                    try{
                        tmp = sc.nextLine();
                        fdn = new SimpleDateFormat("dd/MM/yyyy").parse(tmp);
                    }catch(Exception e){System.out.println("Error");}
                    tmp = null;
                    System.out.println("Ingrese el email del contacto");
                    try{
                        tmp = sc.nextLine();
                    }catch(Exception e){System.out.println("Error");}
                    email = tmp;
                    tmp = null;
                    System.out.println("Ingrese el numero de telefono del contacto");
                    try{
                        tmp = sc.nextLine();
                    }catch(Exception e){System.out.println("Error");}
                    telefono = tmp;

                    contacto = new Contacto(nombre, fdn, email, telefono);
                    agenda.addToHead(contacto);
                break;
                case 2://Borrar
                break;
                case 3://Actualizar
                break;
                case 4://Buscar
                break;
                case 5://Salir
                return;
                default://Error
                break;
            }
        }
    }
}